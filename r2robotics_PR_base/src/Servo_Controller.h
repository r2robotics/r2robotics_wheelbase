//
// Created by ksyy on 26/12/16.
//

#ifndef r2robotics_PR_base_SERVO_CONTROLLER_H
#define r2robotics_PR_base_SERVO_CONTROLLER_H

#include <ros/ros.h>
#include <string>

#include "../common/servomotors_calibration.h"


// Message to communicate with the servos on the Teensy
#include "r2robotics_msgs/ServoRawMsg.h"

// Services provided by the class
#include "r2robotics_srvs/eep_write.h"
#include "r2robotics_srvs/eep_read.h"
#include "r2robotics_srvs/ram_write.h"
#include "r2robotics_srvs/ram_read.h"
#include "r2robotics_srvs/i_jog.h"
#include "r2robotics_srvs/s_jog.h"
#include "r2robotics_srvs/stat.h"
#include "r2robotics_srvs/rollback.h"
#include "r2robotics_srvs/reboot.h"
#include "r2robotics_srvs/activate.h"
#include "r2robotics_srvs/deactivate.h"
#include "r2robotics_srvs/move.h"
#include "r2robotics_srvs/move_simultaneous.h"
#include "r2robotics_srvs/get_position.h"
#include "r2robotics_srvs/clear_errors.h"


class ServoController
{
public:
	ServoController();
	ServoController( std::string servoRequestTopic, std::string servoAnswerTopic );

	void registerServices( ros::NodeHandle *nh, std::string prefix = "" );


	bool eepWrite( r2robotics_srvs::eep_write::Request  &req,
	               r2robotics_srvs::eep_write::Response &answer );

	bool eepRead( r2robotics_srvs::eep_read::Request  &req,
	              r2robotics_srvs::eep_read::Response &answer );

	bool ramWrite( r2robotics_srvs::ram_write::Request  &req,
	               r2robotics_srvs::ram_write::Response &answer );

	bool ramRead( r2robotics_srvs::ram_read::Request  &req,
	              r2robotics_srvs::ram_read::Response &answer );

	bool iJog( r2robotics_srvs::i_jog::Request  &req,
	           r2robotics_srvs::i_jog::Response &answer );

	bool sJog( r2robotics_srvs::s_jog::Request  &req,
	           r2robotics_srvs::s_jog::Response &answer );

	bool stat( r2robotics_srvs::stat::Request  &req,
	           r2robotics_srvs::stat::Response &answer );

	bool rollback( r2robotics_srvs::rollback::Request  &req,
	               r2robotics_srvs::rollback::Response &answer );

	bool reboot( r2robotics_srvs::reboot::Request  &req,
	             r2robotics_srvs::reboot::Response &answer );

	bool activate( r2robotics_srvs::activate::Request  &req,
	               r2robotics_srvs::activate::Response &answer );

	bool deactivate( r2robotics_srvs::deactivate::Request  &req,
	                 r2robotics_srvs::deactivate::Response &answer );

	bool move( r2robotics_srvs::move::Request  &req,
	           r2robotics_srvs::move::Response &answer );

	bool moveSimultaneous( r2robotics_srvs::move_simultaneous::Request  &req,
	                       r2robotics_srvs::move_simultaneous::Response &answer );

	bool getPosition( r2robotics_srvs::get_position::Request  &req,
	                  r2robotics_srvs::get_position::Response &answer );

	bool clearErrors( r2robotics_srvs::clear_errors::Request  &req,
	                  r2robotics_srvs::clear_errors::Response &answer );

private:

	static constexpr ArrayDouble< 1024 > ServoValues[ 3 ]{ makeInterpolationArray< 1024 >( servo1CalibrationIndices, servo1CalibrationValues ),
	                                                       makeInterpolationArray< 1024 >( servo2CalibrationIndices, servo2CalibrationValues ),
	                                                       makeInterpolationArray< 1024 >( servo3CalibrationIndices, servo3CalibrationValues ) };

	// Gets and copies answer from the Teensy
	void processAnswer( const r2robotics_msgs::ServoRawMsg::ConstPtr &msg );
	std::vector< uint8_t > _lastAnswerReceived;

	// Decodes status messages
	void decodeStatus( const uint8_t error, const uint8_t details, r2robotics_msgs::Status &status );

	int stepFromAngle( const int32_t servoId, const float angleDeg ) const;

	std::vector< ros::ServiceServer > _servers;

	ros::NodeHandle* _nodeHandle;
	r2robotics_msgs::ServoRawMsg _servoRawMsg;
	ros::Publisher _servoRequest;
	ros::Subscriber _servoAnswer;

	std::string _servoRequestTopic;
	std::string _servoAnswerTopic;
};


#endif //r2robotics_PR_base_SERVO_CONTROLLER_H
