//
// Created by ksyy on 09/01/17.
//

#ifndef r2robotics_PR_base_MOTOR_CONTROLLER_H
#define r2robotics_PR_base_MOTOR_CONTROLLER_H

#include <ros/ros.h>
#include <string>

// Message to communicate with the motors on the Teensy
#include "r2robotics_msgs/motors_feedback_answer.h"
#include "r2robotics_msgs/motors_feedback_request.h"
#include "r2robotics_msgs/motors_speed_request.h"

// Services provided by the class
#include "r2robotics_srvs/motors_disable.h"
#include "r2robotics_srvs/motors_control.h"
#include "r2robotics_srvs/motors_speed.h"
#include "r2robotics_srvs/motors_temp.h"


class MotorController
{
public:
	MotorController();

	void registerServices( ros::NodeHandle *nh );


	bool disable( r2robotics_srvs::motors_disable::Request  &req,
	              r2robotics_srvs::motors_disable::Response &answer );

	bool control( r2robotics_srvs::motors_control::Request  &req,
	              r2robotics_srvs::motors_control::Response &answer );

	bool getSpeed( r2robotics_srvs::motors_speed::Request  &req,
	               r2robotics_srvs::motors_speed::Response &answer );

	bool getTemp( r2robotics_srvs::motors_temp::Request  &req,
	              r2robotics_srvs::motors_temp::Response &answer );

private:
	// Gets and copies answer from the Teensy
	void processAnswer( const r2robotics_msgs::motors_feedback_answer::ConstPtr &msg );
	std::vector< uint16_t > _lastAnswerReceived;

	std::vector< ros::ServiceServer > _servers;

	ros::NodeHandle* _nodeHandle;
	r2robotics_msgs::motors_speed_request _motorsSpeedRequestMsg;
	r2robotics_msgs::motors_feedback_request _motorsFeedbackRequestMsg;
	r2robotics_msgs::motors_feedback_answer _motorsFeedbackAnswerMsg;
	ros::Publisher _motorsControl;
	ros::Publisher _motorsFeedbackRequest;
	ros::Subscriber _motorsFeedbackAnswer;

	std::string _motorsControlTopic;
	std::string _motorsFeedbackRequestTopic;
	std::string _motorsFeedbackAnswerTopic;
};


#endif //r2robotics_PR_base_MOTOR_CONTROLLER_H
