//
// Created by ksyy on 06/01/17
// Modified by Ksyy on 18/02/18
//

#ifndef R2robotics_PR_base_AS5040_H
#define R2robotics_PR_base_AS5040_H

#include <Arduino.h>
#include <array>

#include "../common/odo_parameters.h"

// defines for 5 bit _status value
constexpr int AS5040_STATUS_OCF = 0x10;
constexpr int AS5040_STATUS_COF = 0x08;
constexpr int AS5040_STATUS_LIN = 0x04;
constexpr int AS5040_STATUS_MAGINC = 0x02;
constexpr int AS5040_STATUS_MAGDEC = 0x01;

template< unsigned int nbChips >
class AS5040
{
public:
	AS5040( const byte pinCLK, const byte pinCS, const byte pinDO );
	~AS5040();
	
	bool begin();
	
	void read();
	const uint16_t value( const size_t chip ) const;
	const byte status ( const size_t chip ) const;
	const bool valid ( const size_t chip ) const;

private:
	
	static constexpr unsigned int _nbChips{ nbChips };

	byte _pinCLK;
	byte _pinCS;
	byte _pinDO;
	
	// Temporarily stores the bitstream from the chips
	std::array< byte, 17 * nbChips > _readingArray;

	// Measurement values and working status of the chips
	std::array< uint16_t, nbChips > _values;
	std::array< byte, nbChips > _status;
	std::array< byte, nbChips > _parities;
	
	const byte evenParity( byte val );
};

#include "Driver_AS5040.cpp"
#endif //R2robotics_PR_base_AS5040_H
