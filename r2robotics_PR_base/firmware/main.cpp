//
// Created by Ksyy on 04/12/16.
//

#include <ros.h>

#include <std_msgs/Empty.h>

#include <r2robotics_msgs/motors_feedback_answer.h>
#include <r2robotics_msgs/motors_feedback_request.h>
#include <r2robotics_msgs/motors_speed_request.h>

#include <r2robotics_msgs/ServoRawMsg.h>
#include <r2robotics_msgs/Position.h>

#include "../common/servomotors_calibration.h"
#include "position_computing.h"
#include "Maxon_Controller.h"
#include "../common/common_operators.h"

#include <Arduino.h>



ros::NodeHandle nh;


static constexpr ArrayDouble< 1 > servoValues[ 3 ]{ makeInterpolationArray< 1 >( servo1CalibrationIndices, servo1CalibrationValues ),
    	 			                              	makeInterpolationArray< 1 >( servo2CalibrationIndices, servo2CalibrationValues ),
	       			                                makeInterpolationArray< 1 >( servo3CalibrationIndices, servo3CalibrationValues ) };

// Servomotors
ServoController servoController( Serial2 );

//*****
// Publisher that sends informations about the servomotors.
//*****
uint8_t answerIntern[ HKL_MAX_MSG_SIZE - 5 ];
r2robotics_msgs::ServoRawMsg answer;
ros::Publisher servoAnswer( "servos_tourelles_ans", &answer );
//*****

//*****
// Subscriber that receives the orders for servomotors and handles them.
//****
void servoMsgCallback( const r2robotics_msgs::ServoRawMsg& cmd_msg )
{
	if( cmd_msg.data_length >= 2 )
	{
		servoController.sendMessage( cmd_msg.data[ 0 ], cmd_msg.data[ 1 ], &( cmd_msg.data[ 2 ] ),
		                             cmd_msg.data_length - 2 );
		
		if( servoController.waitingAnswer() )
		{
			answer.data_length = servoController.getAnswer( answerIntern );
			answer.data = answerIntern;
			servoAnswer.publish( &answer );
		}
	}
}
ros::Subscriber< r2robotics_msgs::ServoRawMsg > servoHandling( "servos_tourelles_rq", servoMsgCallback );
//***************


// Motors controllers
Motor motors[ 3 ]{ { M1_ENABLE_PIN, M1_SPEED_PIN, M1_DIRECTION_PIN, M1_STOP_PIN, M1_TEMP_PIN, M1_ACTUAL_SPEED_PIN },
                   { M2_ENABLE_PIN, M2_SPEED_PIN, M2_DIRECTION_PIN, M2_STOP_PIN, M2_TEMP_PIN, M2_ACTUAL_SPEED_PIN },
                   { M3_ENABLE_PIN, M3_SPEED_PIN, M3_DIRECTION_PIN, M3_STOP_PIN, M3_TEMP_PIN, M3_ACTUAL_SPEED_PIN } };
                   
//*****
// Publisher that sends motor feedbacks
//*****
uint16_t answerMotorIntern[ 3 ];
r2robotics_msgs::motors_feedback_answer answerMotorMsg;
ros::Publisher motorPub{ "motors_feedback_ans", &answerMotorMsg };
//*****
//*****
// Subscriber that receives the requests for feedbacks from the motors.
//*****
void motorFeedbackMsgCallback( const r2robotics_msgs::motors_feedback_request &req )
{
	for( unsigned int i{ 0 } ; i < req.idMotors_length ; ++i )
		answerMotorIntern[ i ] = req.tempOrSpeed ?
		                           motors[ req.idMotors[ i ] - 1 ].getTemp() :
				                       motors[ req.idMotors[ i ] - 1 ].getActualSpeed();
	
	answerMotorMsg.feedback_length = req.idMotors_length;
	answerMotorMsg.feedback = answerMotorIntern;
	motorPub.publish( &answerMotorMsg );
}
ros::Subscriber< r2robotics_msgs::motors_feedback_request >
		motorFeedbackHandling( "motors_feedback_rq", motorFeedbackMsgCallback );

// HACK TO PREVENT MOTOR'S SHAKING
int32_t motorSpeeds[ 3 ];
//*****
//*****
// Subscriber that receives the control requests for the motors.
//*****
void motorControlMsgCallback( const r2robotics_msgs::motors_speed_request &req )
{
	for( unsigned int i{ 0 } ; i < req.idMotors_length ; ++i )
	{
		if( req.enables[ i ] )
		{
			motors[ req.idMotors[ i ] - 1 ].enable();
			motors[ req.idMotors[ i ] - 1 ].unstop();
			motors[ req.idMotors[ i ] - 1 ].setDirection( req.directions[ i ] ? Direction::CCW : Direction::CW );
			motors[ req.idMotors[ i ] - 1 ].setSpeed( req.speeds[ i ] );

			motorSpeeds[ req.idMotors[ i ] - 1 ] = req.speeds[ i ];
		}
		else
		{
			motors[ req.idMotors[ i ] - 1 ].stop();
			motors[ req.idMotors[ i ] - 1 ].disable();

			motorSpeeds[ req.idMotors[ i ] - 1 ] = 0;
		}
	}
}
ros::Subscriber< r2robotics_msgs::motors_speed_request >
    motorControlHandling( "motors_speed_request", motorControlMsgCallback );
//*****

// Odometers
PositionComputing< 3 > positionComputing{};

float rawSpeeds[ 3 ];
std::array< byte, 2 > pos{ HKL_RAM_CALIBRATED_POSITION, 2 };
r2robotics_msgs::Position positionMsg;
ros::Publisher positionPub( "odometry_feedback", &positionMsg );
//*****
// Subscriber that receives orders to reset the internal position of the robot.
//*****
void positionMsgCallback( const r2robotics_msgs::Position& positionToSet )
{
	positionComputing.computePosition();
	positionComputing.setPosition( positionToSet.x, positionToSet.y, positionToSet.theta );
	for( int i{ 0 } ; i < 3 ; ++i )
	{
		servoController.sendMessage( i + 1, HKL_RAM_READ, pos.data(), pos.size() );
		delay( 50 );
		servoController.getAnswer( answerIntern );
		positionComputing.setTurretAngleDiff( static_cast< double >( - servoValuesInt( i, answerIntern[ 4 ] + ( answerIntern[ 5 ] - 96 ) * 256 ) ), i );
	}
	positionComputing.resetTotalTicksTravelled();
}
ros::Subscriber< r2robotics_msgs::Position > positionSetting( "servos_tourelles_setPosition",
                                                                        positionMsgCallback );
//*****


IntervalTimer mainLoopTimer{};
constexpr unsigned long mainLoopPeriod{ 10000 }; // μs
volatile bool sync{ false };


//*****
// Subscriber for the heartbeat signal
//*****
int heartbeatCounter = 10;
void heartbeatCallback( const std_msgs::Empty& empty )
{
	heartbeatCounter = 10;
}
ros::Subscriber< std_msgs::Empty > heartbeatSub( "heartbeat", heartbeatCallback );
//*****

int syncPosition = 0;
void syncing()
{
	sync = true;
}

//AS5040< 6 > odoChain{ ODO_CHAIN_CLK, ODO_CHAIN_CS, ODO_CHAIN_DO };

void setup()
{
	// Begin comm' at the default baudrate
	servoController.begin( 115200 );
	// Change baudrate to 500000 bauds
	std::array< byte, 3 > changeBaudrate{ HKL_REG_BAUD_RATE, 1, 3 };
	servoController.sendMessage( HKL_ALL_ADDRESS, HKL_EEP_WRITE, changeBaudrate.data(), changeBaudrate.size() );
	// Reboot to apply new baudrate
	servoController.sendMessage( HKL_ALL_ADDRESS, HKL_REBOOT, nullptr, 0 );
	servoController.begin( 500000 );
	// Clear status errors that may occur if baudrate was previously set
	std::array< byte, 4 > clearErrors{ HKL_RAM_STATUS_ERROR, 2, 0, 0 };
	servoController.sendMessage( HKL_ALL_ADDRESS, HKL_RAM_WRITE, clearErrors.data(), clearErrors.size() );
	// Set the 0 position of the servomotors
	std::array< byte, 3 > posServo1{ HKL_REG_CALIBRATION_DIFFERENCE, 1, 4 }; // 
	std::array< byte, 3 > posServo2{ HKL_REG_CALIBRATION_DIFFERENCE, 1, 0 }; // 
	std::array< byte, 3 > posServo3{ HKL_REG_CALIBRATION_DIFFERENCE, 1, 255 }; //
	servoController.sendMessage( 1, HKL_EEP_WRITE, posServo1.data(), posServo1.size() );
	servoController.sendMessage( 2, HKL_EEP_WRITE, posServo2.data(), posServo2.size() );
	servoController.sendMessage( 3, HKL_EEP_WRITE, posServo3.data(), posServo3.size() );
	// Reboot to apply
	servoController.sendMessage( HKL_ALL_ADDRESS, HKL_REBOOT, nullptr, 0 );
	// Start the servomotors
	delay( 500 );
	std::array< byte, 3 > activate{ HKL_RAM_TORQUE_CONTROL, 1, 0x60 };
	servoController.sendMessage( HKL_ALL_ADDRESS, HKL_RAM_WRITE, activate.data(), activate.size() );

	positionMsg.rawSpeeds_length = 3;

	positionComputing.computePosition();
	delay( 5 );
	positionComputing.computePosition();
	positionComputing.setPosition( 0, 0, 0 );
	// Get the turret orientation at startup
	for( int i{ 1 } ; i < 4 ; ++i )
	{
		servoController.sendMessage( i, HKL_RAM_READ, pos.data(), pos.size() );
		delay( 50 );
		servoController.getAnswer( answerIntern );
		positionComputing.setTurretAngleDiff( static_cast< double >( - servoValuesInt( i - 1, answerIntern[ 4 ] + ( answerIntern[ 5 ] - 96 ) * 256 ) ), i - 1 );
	}
	positionComputing.computePosition();
	positionComputing.setPosition( 0, 0, 0 );
	
	for( int i{ 0 } ; i < 3 ; ++i )
	{
		motors[ i ].enable();
		motors[ i ].unstop();
		motors[ i ].setDirection( Direction::CW );
		motors[ i ].setSpeed( 0 );
		delay( 100 );
		motors[ i ].stop();
		motors[ i ].disable();
	}

	nh.initNode();
	nh.advertise( servoAnswer );
	nh.subscribe( servoHandling );

	nh.advertise( motorPub );
	nh.subscribe( motorFeedbackHandling );
	nh.subscribe( motorControlHandling );
	
	nh.advertise( positionPub );
	nh.subscribe( positionSetting );
	
	nh.subscribe( heartbeatSub );
	
	servoController.clearSerial();
	
	
	motorSpeeds[ 0 ] = motorSpeeds[ 1 ] = motorSpeeds[ 2 ] = 0;

	servoController.clearSerial();
	
	mainLoopTimer.begin( syncing, mainLoopPeriod );
	mainLoopTimer.priority( 0 );
}

void loop()
{
	while( !sync ) delayMicroseconds(1);
	
	nh.spinOnce();

	/// Emergency stop in case of USB communication failure
	if( heartbeatCounter-- < 0 )
	{
		for( int i{ 0 } ; i < 3 ; ++i )
		{
			if( motors[ i ].isEnabled() )
			{
				motors[ i ].unstop();
				motors[ i ].setDirection( Direction::CW );
				motors[ i ].setSpeed( 0 );
			}
			motorSpeeds[ i ] = 0;
		}
	}
	
	
	positionComputing.computePosition();
	if( ( syncPosition % 10 ) == 0 )
	{
		servoController.sendMessage( syncPosition / 10 + 1, HKL_RAM_READ, pos.data(), pos.size() );
		servoController.getAnswer( answerIntern );
		double angleDiff{ R2Robotics::constrainModulo( 0.0, 6.2832, - static_cast< double >( servoValuesInt( syncPosition / 10, answerIntern[ 4 ] + ( answerIntern[ 5 ] - 96 ) * 256 ) ), 0.0 ) };
		if( R2Robotics::constrainModulo( 0.0, 6.2832, positionComputing.getTurretAngleDiff( syncPosition / 10 ) - angleDiff, 0.0 ) > 0.02 )
			positionComputing.setTurretAngleDiff( angleDiff, syncPosition / 10 );
	}
	if( syncPosition++ == 29)
		syncPosition = 0;
	
	positionMsg.x = static_cast< float > ( positionComputing.getX() );
	positionMsg.y = static_cast< float > ( positionComputing.getY() );
	positionMsg.theta = static_cast< float > ( positionComputing.getTheta() );
	
	rawSpeeds[ 0 ] = static_cast< float > ( positionComputing.getTurretDistSinceLastCall( 0 ) / ( static_cast< double >( mainLoopPeriod ) / 1000000. ) );
	rawSpeeds[ 1 ] = static_cast< float > ( positionComputing.getTurretDistSinceLastCall( 1 ) / ( static_cast< double >( mainLoopPeriod ) / 1000000. ) );
	rawSpeeds[ 2 ] = static_cast< float > ( positionComputing.getTurretDistSinceLastCall( 2 ) / ( static_cast< double >( mainLoopPeriod ) / 1000000. ) );
	
	/*rawSpeeds[ 3 ] = static_cast< float >( positionComputing.getTurretX( 0 ) );
	rawSpeeds[ 4 ] = static_cast< float >( positionComputing.getTurretY( 0 ) );
	rawSpeeds[ 5 ] = static_cast< float >( positionComputing.getTurretAngleAbsolute( 0 ) );
	rawSpeeds[ 6 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 0 ) );
	rawSpeeds[ 7 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 1 ) );
	rawSpeeds[ 8 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 0 ) );
	rawSpeeds[ 9 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 1 ) );
	rawSpeeds[ 10 ] = static_cast< float >( positionComputing.getOdoStatus( 0 ) );
	rawSpeeds[ 11 ] = static_cast< float >( positionComputing.getOdoStatus( 1 ) );
	rawSpeeds[ 12 ] = static_cast< float >( positionComputing.getTurretX( 1 ) );
	rawSpeeds[ 13 ] = static_cast< float >( positionComputing.getTurretY( 1 ) );
	rawSpeeds[ 14 ] = static_cast< float >( positionComputing.getTurretAngleAbsolute( 1 ) );
	rawSpeeds[ 15 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 2 ) );
	rawSpeeds[ 16 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 3 ) );
	rawSpeeds[ 17 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 2 ) );
	rawSpeeds[ 18 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 3 ) );
	rawSpeeds[ 19 ] = static_cast< float >( positionComputing.getOdoStatus( 2 ) );
	rawSpeeds[ 20 ] = static_cast< float >( positionComputing.getOdoStatus( 3 ) );
	rawSpeeds[ 21 ] = static_cast< float >( positionComputing.getTurretX( 2 ) );
	rawSpeeds[ 22 ] = static_cast< float >( positionComputing.getTurretY( 2 ) );
	rawSpeeds[ 23 ] = static_cast< float >( positionComputing.getTurretAngleAbsolute( 2 ) );
	rawSpeeds[ 24 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 4 ) );
	rawSpeeds[ 25 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 5 ) );
	rawSpeeds[ 26 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 4 ) );
	rawSpeeds[ 27 ] = static_cast< float >( positionComputing.getOdoCurrentStep( 5 ) );
	rawSpeeds[ 28 ] = static_cast< float >( positionComputing.getOdoStatus( 4 ) );
	rawSpeeds[ 29 ] = static_cast< float >( positionComputing.getOdoStatus( 5 ) );*/
	
	positionMsg.rawSpeeds = rawSpeeds;
	positionPub.publish( &positionMsg );

	// HACK TO PREVENT THE MOTORS FROM SHAKING
	if( motorSpeeds[ 0 ] == 0 && motorSpeeds[ 1 ] == 0 && motorSpeeds[ 2 ] == 0 &&
	    rawSpeeds[ 0 ] == 0 && rawSpeeds[ 1 ] == 0 && rawSpeeds[ 2 ] == 0 )
	{
		for( int i{ 0 } ; i < 3 ; ++i )
		{
			motors[ i ].stop();
			motors[ i ].disable();
		}
	}

	servoController.clearSerial();
	
	sync = false;
}
