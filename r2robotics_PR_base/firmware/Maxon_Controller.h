#ifndef R2robotics_PR_base_MAXON_CONTROLLER_H
#define R2robotics_PR_base_MAXON_CONTROLLER_H

#include <Arduino.h>
#include "Maxon_Pins.h"


class Motor
{
private:
	uint8_t _enablePin, _speedPin, _directionPin, _stopPin, _tempPin, _actualSpeedPin;
	Direction _direction; // false : clockwise, true : counterclockwise
	bool _stopped; // true : stopped
	bool _enabled; // true : enabled, false : disabled

public:
	Motor( const uint8_t enablePin, const uint8_t speedPin, const uint8_t directionPin, const uint8_t stopPin,
	       const uint8_t tempPin, const uint8_t actualSpeedPin );
	
	void enable();
	void disable();
	bool isEnabled();
	
	void stop();
	void unstop();
	bool isStopped();
	
	uint16_t setSpeed( const uint32_t speed ); // RPM, returns -1 if the speed is in a invalid range
	
	void setDirection( const Direction new_direction );
	bool getDirection();
	
	uint16_t getActualSpeed(); // RPM
	
	uint16_t getTemp(); // Celsius centidegrees
};

#endif //R2robotics_PR_base_MAXON_CONTROLLER_H
