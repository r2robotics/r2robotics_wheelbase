//
// Created by ksyy on 04/12/16.
//

#include "Servo_Controller.h"


ServoController::ServoController( decltype( Serial1 )& serialInterface )
	: _serialInterface( serialInterface )
{
//	clearSerial();
}

ServoController::~ServoController()
{
	_serialInterface.end();
}


void ServoController::begin( const int baudrate )
{
	_serialInterface.end();
	_serialInterface.begin( baudrate );
}

void ServoController::sendMessage( const byte address, const byte command, const byte* data, const int dataSize )
{
	// Eliminate speed control commands
	if( ( address < 3 || address == HKL_ALL_ADDRESS ) && ( command == HKL_I_JOG || command == HKL_S_JOG ) )
			for( int i{ command == HKL_I_JOG ? 2 : 3 } ; i < dataSize ; i += command == HKL_I_JOG ? 5 : 4 )
				if( data[ i ] & 2 )
					return;
	
	// Header
	_message[ 0 ] = _message[ 1 ] = HKL_HEADER;
	// Packet Size
	_message[ 2 ] = 7 + dataSize;
	// Address
	_message[ 3 ] = address;
	// Command
	_message[ 4 ] = command;
	// Data
	for( int i{ 0 } ; i < dataSize ; ++i )
		_message[ 7 + i ] = data[ i ];
	
	// Checksums
	addChecksums( _message, dataSize + 7 );
	
	// Sending
	_serialInterface.write( _message, 7 + dataSize );
	
	if( command == HKL_EEP_READ || command == HKL_RAM_READ || command == HKL_STAT )
	{
		// If _nextAnswerWrite points to a used value, something wrong happened (no response)
		_serialInterface.flush();
		_answerSizes[ _nextAnswerWrite++ ] = command == HKL_STAT ? 9 : data[ 1 ] + 11;
		if( _nextAnswerWrite == ANSWER_QUEUE_SIZE )
			_nextAnswerWrite = 0;
	}
}

void ServoController::addChecksums( byte* message, const int messageSize )
{
	// The cast eliminates a narrowing warning (^ convert to int).
	byte checksum1{ static_cast< byte >( ( message[ 2 ] ^ message[ 3 ] ) ^ message[ 4 ] ) };
	
	for( int i{ 7 }; i < messageSize ; ++i )
		checksum1 ^= message[ i ];
		
	message[ 5 ] = checksum1 &= HKL_CHECKSUM;
	message[ 6 ] = ~checksum1 & HKL_CHECKSUM;
}

bool ServoController::checksums( byte* message, const int messageSize )
{
	byte checksum1{ message[ 5 ] }, checksum2{ message[ 6 ] };
	
	addChecksums( message, messageSize );
	
	return ( checksum1 == message[ 5 ] ) && ( checksum2 == message[ 6 ] );
}

const int ServoController::available()
{
	return _serialInterface.available();
}

const int ServoController::getAnswer( byte* answer )
{
	// If we do not expect a reading...
	if( _answerSizes[ _nextAnswerRead ] == 0 )
		return 0;
	
	// Awaiting message reception...
	for( int i{ 0 } ; i < 500 && _serialInterface.available() < _answerSizes[ _nextAnswerRead ] ; ++i )
		delayMicroseconds( 10 );
	
	auto answerSize = _answerSizes[ _nextAnswerRead ];
	_answerSizes[ _nextAnswerRead++ ] = 0;
	if( _nextAnswerRead == ANSWER_QUEUE_SIZE )
		_nextAnswerRead = 0;
	
	// If the message has not been received after 5ms, something is wrong, we abort this reading.
	if( _serialInterface.available() < answerSize )
		return 0;
	
	
	// Reading
	for( size_t i{ 0 } ; i < answerSize ; ++i )
		_answer[ i ] = _serialInterface.read();
	
	// Checks the headers and the size of the message
	if( _answer[ 0 ] != HKL_HEADER || _answer[ 1 ] != HKL_HEADER || _answer[ 2 ] != answerSize )
		return 0;
	
	
	// Checks the checksums
	if( !checksums( _answer, _answer[ 2 ] ) )
		return 0;
	
	// Gets the necessary information
	answer[ 0 ] = _answer[ 3 ];
	answer[ 1 ] = _answer[ 4 ];
	for( int count{ 7 } ; count < _answer[ 2 ] ; ++count )
		answer[ count - 5 ] = _answer[ count ];
	
	return _answer[ 2 ] - 5;
}

void ServoController::clearSerial()
{
	_nextAnswerRead = _nextAnswerWrite = 0;
	
	for( size_t i{ 0 } ; i < ANSWER_QUEUE_SIZE ; ++i )
		_answerSizes[ i ] = 0;
	
	while( _serialInterface.available() )
		_serialInterface.read();
}

const bool ServoController::waitingAnswer()
{
	return _answerSizes[ _nextAnswerRead ] != 0;
}