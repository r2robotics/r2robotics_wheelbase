//
// Created by ksyy on 04/12/16.
//

#ifndef R2robotics_PR_base_SERVO_CONTROLLER_H
#define R2robotics_PR_base_SERVO_CONTROLLER_H

#include <Arduino.h>

#include "../common/herkulex_keywords.h"


class ServoController
{
	void addChecksums( byte* message, const int messageSize );
	bool checksums( byte* message, const int messageSize );
	
	decltype( Serial1 )& _serialInterface;
	byte _message[ HKL_MAX_MSG_SIZE ];
	byte _answer[ HKL_MAX_MSG_SIZE ];
	
	static const size_t ANSWER_QUEUE_SIZE{ 10 };
	uint8_t _answerSizes[ ANSWER_QUEUE_SIZE ]; // The size of the queued answers
	size_t _nextAnswerRead, _nextAnswerWrite;
	
public:
  
  ServoController( decltype( Serial1 )& serialInterface );
  ~ServoController();
  
	void begin( const int baudrate );
	
	void sendMessage( const byte address, const byte command, const byte* data, const int dataSize );
	
	const int available();
	
	const int getAnswer( byte* answer );
	
	void clearSerial();
	
	const bool waitingAnswer();
};

#endif //R2robotics_PR_base_SERVO_CONTROLLER_H
