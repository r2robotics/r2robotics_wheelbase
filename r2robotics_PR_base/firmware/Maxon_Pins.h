#ifndef R2robotics_PR_base_MAXON_PINS_H
#define R2robotics_PR_base_MAXON_PINS_H

// MOTOR 1
const uint8_t M1_ENABLE_PIN{ 21 };
const uint8_t M1_SPEED_PIN{ 20 };
const uint8_t M1_DIRECTION_PIN{ 22 };
const uint8_t M1_STOP_PIN{ 23 };
// ANALOG IN A*
const uint8_t M1_TEMP_PIN{ 15 };
const uint8_t M1_ACTUAL_SPEED_PIN{ 14 };

// MOTOR 2
// DIGITAL OUT D*
const uint8_t M2_ENABLE_PIN{ 4 };
const uint8_t M2_SPEED_PIN{ 3 };
const uint8_t M2_DIRECTION_PIN{ 11 };
const uint8_t M2_STOP_PIN{ 12 };
// ANALOG IN A*
const uint8_t M2_TEMP_PIN{ 17 };
const uint8_t M2_ACTUAL_SPEED_PIN{ 16 };

// MOTOR 3
// DIGITAL OUT D*
const uint8_t M3_ENABLE_PIN{ 8 };
const uint8_t M3_SPEED_PIN{ 5 };
const uint8_t M3_DIRECTION_PIN{ 7 };
const uint8_t M3_STOP_PIN{ 6 };
// ANALOG IN A*
const uint8_t M3_TEMP_PIN{ 19 };
const uint8_t M3_ACTUAL_SPEED_PIN{ 18 };


// CONVENTION FOR MOTOR DIRECTION
enum Direction : bool
{
	CCW = true,
	CW = false,
}; // High = Counterclockwise

// COEFFICIENTS FOR ANALOG INPUTS
const uint16_t TEMP_VOLTAGE_MIN{ 0 }; // Millivolts
const uint16_t TEMP_VALUE_MIN{ 0 }; // Celsius centidegrees
const uint16_t TEMP_VOLTAGE_MAX{ 3000 }; // Millivolts
const uint16_t TEMP_VALUE_MAX{ 15000 }; // Celsius centidegrees

const uint16_t ACTUAL_SPEED_VOLTAGE_MIN{ 1500 }; // Millivolts
const uint16_t ACTUAL_SPEED_VALUE_MIN{ 0 }; // RPM
const uint16_t ACTUAL_SPEED_VOLTAGE_MAX{ 3000 }; // Millivolts
const uint16_t ACTUAL_SPEED_VALUE_MAX{ 3000 }; // RPM

// PWM PARAMETERS
const uint8_t PWM_RESOLUTION{ 16 }; // bits
const uint32_t TEENSY_MAX_PWM{ 65536 - 1 }; // 2^PWM_RESOLUTION - 1

// ANALOG READ PARAMETERS
const uint8_t ANALOG_RESOLUTION{ 12 };
const uint32_t ANALOG_MAX_VALUE{ 4096 - 1 };
const uint32_t ANALOG_MAX_MILLIVOLTS{ 3300 };
const uint8_t ANALOG_SAMPLES_AVERAGING{ 4 };


// COEFFICIENTS FOR PWM SPEED CONTROL
const uint16_t SPEED_PWM_PERCENT_MIN{ 10 }; // percent
const uint16_t SPEED_VALUE_MIN{ 0 }; // RPM
const uint16_t SPEED_PWM_PERCENT_MAX{ 90 }; // percent
const uint16_t SPEED_VALUE_MAX{ 2500 }; // RPM

const uint16_t TEENSY_MIN_SPEED_PWM{ static_cast< uint32_t >( TEENSY_MAX_PWM ) *
                                     static_cast< uint32_t >( SPEED_PWM_PERCENT_MIN ) / 100 };
const uint16_t TEENSY_MAX_SPEED_PWM{ static_cast< uint32_t >( TEENSY_MAX_PWM ) *
                                     static_cast< uint32_t >( SPEED_PWM_PERCENT_MAX ) / 100 };


template< typename T >
constexpr uint32_t RANGE( const T max, const T min ){ return static_cast< uint32_t >( max ) -
                                                             static_cast< uint32_t >( min ); };
#endif //R2robotics_PR_base_MAXON_PINS_H
