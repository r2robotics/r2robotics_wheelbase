//
// Created by Ksyy on 04/12/16.
//

#include <ros.h>

#include <std_msgs/Empty.h>

#include <r2robotics_msgs/motors_feedback_answer.h>
#include <r2robotics_msgs/motors_feedback_request.h>
#include <r2robotics_msgs/motors_speed_request.h>

#include <r2robotics_msgs/Position.h>

#include "position_computing.h"
#include "Maxon_Controller.h"
#include "../common/common_operators.h"

#include <Arduino.h>



ros::NodeHandle nh;

// Motors controllers
Motor motors[ 2 ]{ { M1_ENABLE_PIN, M1_SPEED_PIN, M1_DIRECTION_PIN, M1_STOP_PIN, M1_TEMP_PIN, M1_ACTUAL_SPEED_PIN },
                   { M2_ENABLE_PIN, M2_SPEED_PIN, M2_DIRECTION_PIN, M2_STOP_PIN, M2_TEMP_PIN, M2_ACTUAL_SPEED_PIN } };
                   
//*****
// Publisher that sends motor feedbacks
//*****
uint16_t answerMotorIntern[ 2 ];
r2robotics_msgs::motors_feedback_answer answerMotorMsg;
ros::Publisher motorPub{ "motors_feedback_ans", &answerMotorMsg };
//*****
//*****
// Subscriber that receives the requests for feedbacks from the motors.
//*****
void motorFeedbackMsgCallback( const r2robotics_msgs::motors_feedback_request &req )
{
	for( unsigned int i{ 0 } ; i < req.idMotors_length ; ++i )
		answerMotorIntern[ i ] = req.tempOrSpeed ?
		                           motors[ req.idMotors[ i ] - 1 ].getTemp() :
				                       motors[ req.idMotors[ i ] - 1 ].getActualSpeed();
	
	answerMotorMsg.feedback_length = req.idMotors_length;
	answerMotorMsg.feedback = answerMotorIntern;
	motorPub.publish( &answerMotorMsg );
}
ros::Subscriber< r2robotics_msgs::motors_feedback_request >
		motorFeedbackHandling( "motors_feedback_rq", motorFeedbackMsgCallback );

// HACK TO PREVENT MOTOR'S SHAKING
int32_t motorSpeeds[ 2 ];
//*****
//*****
// Subscriber that receives the control requests for the motors.
//*****
void motorControlMsgCallback( const r2robotics_msgs::motors_speed_request &req )
{
	for( unsigned int i{ 0 } ; i < req.idMotors_length ; ++i )
	{
		if( req.enables[ i ] )
		{
			motors[ req.idMotors[ i ] - 1 ].enable();
			motors[ req.idMotors[ i ] - 1 ].unstop();
			motors[ req.idMotors[ i ] - 1 ].setDirection( req.directions[ i ] ? Direction::CCW : Direction::CW );
			motors[ req.idMotors[ i ] - 1 ].setSpeed( req.speeds[ i ] );

			motorSpeeds[ req.idMotors[ i ] - 1 ] = req.speeds[ i ];
		}
		else
		{
			motors[ req.idMotors[ i ] - 1 ].stop();
			motors[ req.idMotors[ i ] - 1 ].disable();

			motorSpeeds[ req.idMotors[ i ] - 1 ] = 0;
		}
	}
}
ros::Subscriber< r2robotics_msgs::motors_speed_request >
    motorControlHandling( "motors_speed_request", motorControlMsgCallback );
//*****

// Odometers
PositionComputing< 1 > positionComputing{};

float rawSpeeds[ 2 ];
r2robotics_msgs::Position positionMsg;
ros::Publisher positionPub( "odometry_feedback", &positionMsg );
//*****
// Subscriber that receives orders to reset the internal position of the robot.
//*****
void positionMsgCallback( const r2robotics_msgs::Position& positionToSet )
{
	positionComputing.computePosition();
	positionComputing.setPosition( positionToSet.x, positionToSet.y, positionToSet.theta );
	positionComputing.resetTotalTicksTravelled();
}
ros::Subscriber< r2robotics_msgs::Position > positionSetting( "setPosition",
                                                               positionMsgCallback );
//*****


IntervalTimer mainLoopTimer{};
constexpr unsigned long mainLoopPeriod{ 10000 }; // μs
volatile bool sync{ false };


//*****
// Subscriber for the heartbeat signal
//*****
int heartbeatCounter = 10;
void heartbeatCallback( const std_msgs::Empty& empty )
{
	heartbeatCounter = 10;
}
ros::Subscriber< std_msgs::Empty > heartbeatSub( "heartbeat", heartbeatCallback );
//*****

int syncPosition = 0;
void syncing()
{
	sync = true;
}

//AS5040< 2 > odoChain{ ODO_CHAIN_CLK, ODO_CHAIN_CS, ODO_CHAIN_DO };

void setup()
{
	positionMsg.rawSpeeds_length = 2;

	delay(500);

	positionComputing.computePosition();
	delay( 5 );
	positionComputing.computePosition();
	positionComputing.setPosition( 0, 0, 0 );
	positionComputing.computePosition();
	positionComputing.setPosition( 0, 0, 0 );
	
	for( int i{ 0 } ; i < 2 ; ++i )
	{
		motors[ i ].enable();
		motors[ i ].unstop();
		motors[ i ].setDirection( Direction::CW );
		motors[ i ].setSpeed( 0 );
		delay( 100 );
		motors[ i ].stop();
		motors[ i ].disable();
	}

	nh.initNode();
	nh.advertise( motorPub );
	nh.subscribe( motorFeedbackHandling );
	nh.subscribe( motorControlHandling );
	
	nh.advertise( positionPub );
	nh.subscribe( positionSetting );
	
	nh.subscribe( heartbeatSub );
	
	
	motorSpeeds[ 0 ] = motorSpeeds[ 1 ] = 0;

	mainLoopTimer.begin( syncing, mainLoopPeriod );
	mainLoopTimer.priority( 0 );
}

void loop()
{
	while( !sync ) delayMicroseconds(1);
	
	nh.spinOnce();

	/// Emergency stop in case of USB communication failure
	if( heartbeatCounter-- < 0 )
	{
		for( int i{ 0 } ; i < 2 ; ++i )
		{
			if( motors[ i ].isEnabled() )
			{
				motors[ i ].unstop();
				motors[ i ].setDirection( Direction::CW );
				motors[ i ].setSpeed( 0 );
			}
			motorSpeeds[ i ] = 0;
		}
	}
	
	
	positionComputing.computePosition();
	
	positionMsg.x = static_cast< float > ( positionComputing.getX() );
	positionMsg.y = static_cast< float > ( positionComputing.getY() );
	positionMsg.theta = static_cast< float > ( positionComputing.getTheta() );
	
	rawSpeeds[ 0 ] = static_cast< float > ( positionComputing.getOdoDistSinceLastCall( 0 ) / ( static_cast< double >( mainLoopPeriod ) / 1000000. ) );
	rawSpeeds[ 1 ] = static_cast< float > ( positionComputing.getOdoDistSinceLastCall( 1 ) / ( static_cast< double >( mainLoopPeriod ) / 1000000. ) );
	
	/*rawSpeeds[ 2 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 0 ) );
	rawSpeeds[ 3 ] = static_cast< float >( positionComputing.getTotalTicksTravelled( 1 ) );
	rawSpeeds[ 4 ] = static_cast< float >( positionComputing.getOdoStatus( 0 ) );
	rawSpeeds[ 5 ] = static_cast< float >( positionComputing.getOdoStatus( 1 ) );*/
	
	positionMsg.rawSpeeds = rawSpeeds;
	positionPub.publish( &positionMsg );

	// HACK TO PREVENT THE MOTORS FROM SHAKING
	if( motorSpeeds[ 0 ] == 0 && motorSpeeds[ 1 ] == 0 &&
	    rawSpeeds[ 0 ] == 0 && rawSpeeds[ 1 ] == 0 )
	{
		for( int i{ 0 } ; i < 2 ; ++i )
		{
			motors[ i ].stop();
			motors[ i ].disable();
		}
	}

	sync = false;
}
