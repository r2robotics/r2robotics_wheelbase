//
// Created by ksyy on 06/01/17
// Modified by Ksyy on 18/02/18
//

#include <algorithm>
//#include "Driver_AS5040.h"


template< unsigned int nbChips >
AS5040< nbChips >::AS5040( const byte pinCLK, const byte pinCS, const byte pinDO )
  : _pinCLK{ pinCLK }
  , _pinCS{ pinCS }
  , _pinDO{ pinDO }
{}

template< unsigned int nbChips >
AS5040< nbChips >::~AS5040()
{}

template< unsigned int nbChips >
bool AS5040< nbChips >::begin()
{
	pinMode( _pinCS, OUTPUT );
	digitalWrite( _pinCS, HIGH );
	pinMode( _pinCLK, OUTPUT );
	digitalWrite( _pinCLK, HIGH );
	pinMode( _pinDO, INPUT_PULLDOWN );
	
	byte count = 0 ;
	while( read (), std::all_of( _status.begin(), _status.end(), []( byte s ){ return s & AS5040_STATUS_OCF; } ) )
	{
		if (++count > 30) return false ; // failed to initialize
		delay (1) ;
	}
	
	return true ;
}

// read position value, squirrel away status
template< unsigned int nbChips >
void AS5040< nbChips >::read()
{
	_readingArray.fill( 0 );

	digitalWrite( _pinCS, LOW );
	delayMicroseconds( 5 );
	
	for( size_t i{ 0 } ; i < 17 * _nbChips ; ++i )
	{
		digitalWrite( _pinCLK, LOW );
		delayMicroseconds( 5 );
		digitalWrite( _pinCLK, HIGH );
		delayMicroseconds( 5 );
		_readingArray[ i ] =  digitalRead( _pinDO );
	}
	
	digitalWrite( _pinCS, HIGH );
	delayMicroseconds( 5 );
	
	size_t arrayIt{ 0 };
	
	for( size_t chip{ 0 } ; chip < _nbChips ; ++chip )
	{
		uint16_t value{ 0 };
		for( size_t i{ 0 } ; i < 10 ; ++i )
			value = ( value << 1 ) | _readingArray[ arrayIt++ ];
		
		byte status{ 0 };
		for( size_t i{ 0 } ; i < 6 ; ++i )
			status = ( status << 1 ) | _readingArray[ arrayIt++ ];
		
		_parities[ chip ] = evenParity( value >> 2 ) ^ evenParity( value & 3 ) ^ evenParity( status );
		_status[ chip ] = status >> 1 ;
		_values[ chip ] = value;
		
		arrayIt++; // Blank bit between messages.
	}
}

// raw status from latest read, 5 bit value
template< unsigned int nbChips >
const byte AS5040< nbChips >::status( const size_t chip ) const
{
	return _status[ chip ] ;
}

// indicate if latest status implies valid data
template< unsigned int nbChips >
const bool AS5040< nbChips >::valid( const size_t chip ) const 
{
	return _parities[ chip ] == 0 &&
			   ( _status[ chip ] & ( AS5040_STATUS_OCF | AS5040_STATUS_COF | AS5040_STATUS_LIN ) ) == AS5040_STATUS_OCF/* &&
			   ( _status[ chip ] & ( AS5040_STATUS_MAGINC | AS5040_STATUS_MAGDEC ) ) != ( AS5040_STATUS_MAGINC | AS5040_STATUS_MAGDEC ) */;  // Working even if DEC & INC are on, so...
}

template< unsigned int nbChips >
const uint16_t AS5040< nbChips >::value( const size_t chip ) const
{
	return _values[ chip ];
}


template< unsigned int nbChips >
const byte AS5040< nbChips >::evenParity( byte val )
{
	val ^= val >> 1;
	val ^= val >> 2;
	val ^= val >> 4;
	return val & 1;
}