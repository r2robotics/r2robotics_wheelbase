//
// Created by ksyy on 10/01/17
// Modified by Ksyy on 18/02/17
//

#ifndef ROS_POSITION_COMPUTING_H
#define ROS_POSITION_COMPUTING_H

#include <Arduino.h>
#include <array>
#include <tuple>
#include <functional>

#include "../common/odo_parameters.h"
#include "Driver_AS5040.h"
#include "Servo_Controller.h"


typedef double mm;
typedef double rad;

template< unsigned int nbTurrets >
class PositionComputing
{
	class OdoTurret
	{
	public:
		
		OdoTurret( const mm odo1Gain, const mm odo1DistFromCentre, 
				   const mm odo2Gain, const mm odo2DistFromCentre );
		
		bool computePosition( const std::array< int32_t, 2 > values );
		
		const mm getX() const;
		const mm getY() const;
		const rad getAngle() const;
		const mm getOdoDistSinceLastCall( const size_t i );
		
		void setX( const mm x );
		void setY( const mm y );
		void setAngle( const rad angle );

		// Debug functions to set/check odometers' gains
		void resetTotalTicksTravelled();
		const int32_t getTotalTicksTravelled( const size_t i ) const;
        const int32_t getOdoCurrentStep( const size_t i ) const;

	private:
		
		// Position and orientation of the turret, global frame
		mm _x, _y;
		rad _angle;

		// For speed computation
		std::array< mm,  2 > _tempDistTotalOdo;

		// Odometers' parameters
		std::array< const mm, 2 > _odoGain;
		std::array< const mm, 2 > _odoDistFromCentre;
		
		std::array< int32_t, 2 > _odoCurrentStep, _odoLastStep;
		
		// Debug variable to set/check odometers' gains
		std::array< int32_t, 2 > _odoTotalTicks;
	};


public:
	// Turret, init angle, distance and angle from real centre, angle from turret average centre
	using Turret = std::tuple< OdoTurret, rad, mm, rad >;
	
	PositionComputing();

	// Compute all the turret's parameters
	static Turret make_turret( const int id,
							   const auto gains, const auto dists, 
							   const auto x, const auto y, const auto initYaw );


	void setPosition( const mm x, const mm y, const rad theta );
	
	bool computePosition();
	
	// Returns the robot coordinates (center of gravity of the turrets)
	const mm getX() const;
	const mm getY() const;
	const rad getTheta() const;
	
	// Returns turrets coordinates (0 is the default because differential robots)
	const mm getTurretX( const size_t i = 0 ) const;
	const mm getTurretY( const size_t i = 0 ) const;
	const rad getTurretAngleAbsolute( const size_t i = 0 ) const;
	const rad getTurretAngleDiff( const size_t i = 0 ) const;  // Angle given by the servomotor, Angle diff = Angle absolute - Angle wheelbase
	const mm getOdoDistSinceLastCall( const size_t i );
	const mm getTurretDistSinceLastCall( const size_t i = 0 );

	// Updates turrets own angle (0 is the default because differential robots are like 1 turret)
	void setTurretAngleDiff( const rad angle, const size_t i = 0 );

	// Debug functions to set/check odometers' gains
	void resetTotalTicksTravelled();
	const int32_t getTotalTicksTravelled( const size_t i ) const;
	const int32_t getOdoCurrentStep( const size_t i ) const;
	const int32_t getOdoStatus( const size_t i ) const;

private:
	
	const std::tuple< mm, mm > centreTurrets();
	
	mm _x, _y;
	rad _theta;	
	
	AS5040< nbTurrets * 2 > _odoChain;

	// Tuple: Turret, 0 orientation of the turret in the robot frame, distance from the centre of all the turrets to the centre of the turret,
	// angle of the centre of the turret from the centre of all the turrets, in the robot frame
	std::array< Turret, nbTurrets > _turrets;
};

#include "position_computing.cpp"
#endif //ROS_POSITION_COMPUTING_H
