//
// Created by ksyy on 11/01/17.
//

#ifndef ROS_COMMON_OPERATORS_H
#define ROS_COMMON_OPERATORS_H

#include <vector>
#include <array>
#include <algorithm>
#include <numeric>
#include <cmath>

namespace R2Robotics
{
	
	template< typename T >
	constexpr T constrainModulo( const T min, const T max, T value, T step = 0 )
	{
		if( abs(step) <= 0.000001 )
			step = max - min;
		
		while( value >= max )
			value -= step;
		while( value < min )
			value += step;
		
		return value;
	}

	template< typename T >
	constexpr T absoluteValue( const T value )
	{
		if( value < static_cast< T >( 0 ) )
			return -value;
		else
			return value;
	}
	
	template< typename T >
	constexpr T degToRad( const T deg )
	{
		return static_cast< T >( ( 3.141592 * static_cast< double >( deg ) ) / 180. );
	}
	
	template< typename T >
	constexpr T radToDeg( const T rad )
	{
		return static_cast< T >( ( 180. * static_cast< double >( rad ) ) / 3.141592 );
	}
}


#endif //ROS_COMMON_OPERATORS_H
